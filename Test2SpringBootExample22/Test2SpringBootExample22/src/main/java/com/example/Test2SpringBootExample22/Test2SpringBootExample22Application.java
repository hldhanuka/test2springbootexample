package com.example.Test2SpringBootExample22;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test2SpringBootExample22Application {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample22Application.class, args);
	}

}
