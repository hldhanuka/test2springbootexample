package org.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test2SpringBootExample1Application {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample1Application.class, args);
	}

}
