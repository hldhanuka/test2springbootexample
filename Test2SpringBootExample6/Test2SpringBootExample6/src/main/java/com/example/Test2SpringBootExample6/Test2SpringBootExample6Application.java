package com.example.Test2SpringBootExample6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test2SpringBootExample6Application {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample6Application.class, args);
	}

}
