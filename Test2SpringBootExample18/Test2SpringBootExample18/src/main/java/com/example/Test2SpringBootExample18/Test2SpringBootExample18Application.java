package com.example.Test2SpringBootExample18;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test2SpringBootExample18Application {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample18Application.class, args);
	}

}
