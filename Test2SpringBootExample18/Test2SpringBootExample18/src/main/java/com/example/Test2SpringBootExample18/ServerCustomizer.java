package com.example.Test2SpringBootExample18;

import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.stereotype.Component;

@Component
public class ServerCustomizer implements EmbeddedServletContainerCustomizer {
    @Override
    public void customize(ConfigurableEmbeddedServletContainer container) {
        container.setPort(8097);
    }
}

//@Component
//public class ServerCustomizer implements WebServerFactoryCustomizer<ConfigurableWebServerFactory< {
//    @Override
//    public void customize(ConfigurableWebServerFactory factory) {
//        factory.setPort(9001);
//    }
//}
