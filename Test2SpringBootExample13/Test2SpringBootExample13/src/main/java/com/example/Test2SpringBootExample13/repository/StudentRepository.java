package com.example.Test2SpringBootExample13.repository;

import com.example.Test2SpringBootExample13.model.Student;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student, Integer> {
}