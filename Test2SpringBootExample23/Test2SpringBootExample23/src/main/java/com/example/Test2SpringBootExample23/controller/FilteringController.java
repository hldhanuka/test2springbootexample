package com.example.Test2SpringBootExample23.controller;

import java.util.Arrays;
import java.util.List;

import com.example.Test2SpringBootExample23.SomeBean;
import com.example.Test2SpringBootExample23.SomeBean2;
import com.example.Test2SpringBootExample23.SomeBean3;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FilteringController {
    //returning a single bean as response
    @RequestMapping("/filtering")
    public SomeBean retrieveSomeBean() {
        return new SomeBean("Amit", "9999999999", "39000");
    }

    //returning a list of SomeBeans as response
    @RequestMapping("/filtering-list")
    public List<SomeBean2> retrieveListOfSomeBeans() {
        return Arrays.asList(new SomeBean2("Saurabh", "8888888888", "20000"), new SomeBean2("Devesh", "1111111111", "34000"));
    }

    @RequestMapping("/filtering-3")
    public MappingJacksonValue retrieveSomeBean2() {
        SomeBean3 someBean = new SomeBean3("Amit", "9999999999", "39000");

        //invoking static method filterOutAllExcept()
        SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept("name", "salary");

        //creating filter using FilterProvider class
        FilterProvider filters = new SimpleFilterProvider().addFilter("SomeBeanFilter", filter);

        //constructor of MappingJacksonValue class  that has bean as constructor argument
        MappingJacksonValue mapping = new MappingJacksonValue(someBean);

        //configuring filters
        mapping.setFilters(filters);

        return mapping;
    }

    @RequestMapping("/filtering-list-2")
    public MappingJacksonValue retrieveListOfSomeBeans2() {
        List<SomeBean> list = Arrays.asList(new SomeBean("Saurabh", "8888888888", "20000"), new SomeBean("Devesh", "1111111111", "34000"));

        //invoking static method filterOutAllExcept()
        SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept("name", "phone");

        FilterProvider filters = new SimpleFilterProvider().addFilter("SomeBeanFilter", filter);

        //constructor of MappingJacksonValue class that has list as constructor argument
        MappingJacksonValue mapping = new MappingJacksonValue(list);

        //configuring filter
        mapping.setFilters(filters);

        return mapping;
    }
}