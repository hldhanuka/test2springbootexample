package com.example.Test2SpringBootExample23;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test2SpringBootExample23Application {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample23Application.class, args);
	}

}
