package com.example.Test2SpringBootExample26;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test2SpringBootExample26Application {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample26Application.class, args);
	}

}
