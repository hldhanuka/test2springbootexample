package org.example.service.api;

import org.example.domain.entity.Account;

import java.util.List;

public interface AccountService {
    /**
     * Finds the account with the provided account number.
     *
     * @param number The account number
     * @return The account
     * @throws AccountNotFoundException If no such account exists.
     */
    Account findOne(String number) throws AccountNotFoundException;

    /**
     * Creates a new account.
     *
     * @param number
     * @return created account
     */
    Account createAccountByNumber(String number);
}
