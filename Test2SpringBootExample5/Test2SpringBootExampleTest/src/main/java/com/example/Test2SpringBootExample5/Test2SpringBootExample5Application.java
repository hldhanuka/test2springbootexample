package com.example.Test2SpringBootExample5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test2SpringBootExample5Application {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample5Application.class, args);
	}

}
