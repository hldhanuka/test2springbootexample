package com.example.Test2SpringBootExample9.service.impl;

import com.example.Test2SpringBootExample9.model.Account;

//creating interface that throws exception if the customer id not found
public interface AccountService {
    public abstract Account getAccountByCustomerId(String customerId)
            throws Exception;
}