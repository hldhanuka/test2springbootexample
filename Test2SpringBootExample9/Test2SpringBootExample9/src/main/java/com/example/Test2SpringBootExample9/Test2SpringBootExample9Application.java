package com.example.Test2SpringBootExample9;

import com.example.Test2SpringBootExample9.model.Account;
import com.example.Test2SpringBootExample9.service.impl.AccountService;
import com.example.Test2SpringBootExample9.service.impl.AccountServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
//@EnableAspectJAutoProxy annotation enables support for handling the components marked with @Aspect annotation. It is similar to tag in the xml configuration.  
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class Test2SpringBootExample9Application {
    public static void main(String[] args) {
        ConfigurableApplicationContext ac = SpringApplication.run(Test2SpringBootExample9Application.class, args);
        //Fetching the account object from the application context
        AccountService accountService = ac.getBean("accountServiceImpl", AccountServiceImpl.class);
        Account account;
        try {
            account = accountService.getAccountByCustomerId("K2434567");
            if (account != null)
                System.out.println(account.getAccountNumber() + "\t" + account.getAccountType());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}