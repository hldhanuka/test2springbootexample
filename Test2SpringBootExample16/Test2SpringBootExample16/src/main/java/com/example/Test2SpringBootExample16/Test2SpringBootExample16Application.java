package com.example.Test2SpringBootExample16;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class Test2SpringBootExample16Application {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample16Application.class, args);
	}

}
