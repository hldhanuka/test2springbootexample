package com.example.Test2SpringBootExample7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class Test2SpringBootExample7Application extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Test2SpringBootExample7Application.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(Test2SpringBootExample7Application.class, args);
    }
}
