package com.example.Test2SpringBootExample14;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test2SpringBootExample14Application {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample14Application.class, args);
	}

}
