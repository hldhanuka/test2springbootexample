package com.example.Test2SpringBootExample14.repository;

import com.example.Test2SpringBootExample14.model.Books;
import org.springframework.data.repository.CrudRepository;

//repository that extends CrudRepository  
public interface BooksRepository extends CrudRepository<Books, Integer> {
}