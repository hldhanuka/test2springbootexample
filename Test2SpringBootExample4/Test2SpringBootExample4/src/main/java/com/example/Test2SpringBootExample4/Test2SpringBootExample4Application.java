package com.example.Test2SpringBootExample4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test2SpringBootExample4Application {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample4Application.class, args);
	}

}
