package com.example.Test2SpringBootExample4;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoRestController {
    @GetMapping("/hello")
    public String hello() {
        return "Hello User!";
    }
}