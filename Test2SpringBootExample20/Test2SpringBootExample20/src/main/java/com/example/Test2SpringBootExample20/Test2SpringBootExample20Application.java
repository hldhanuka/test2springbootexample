package com.example.Test2SpringBootExample20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test2SpringBootExample20Application {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample20Application.class, args);
	}

}
