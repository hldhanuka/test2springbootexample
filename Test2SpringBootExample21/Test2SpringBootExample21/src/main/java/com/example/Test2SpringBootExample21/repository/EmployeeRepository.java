package com.example.Test2SpringBootExample21.repository;

import com.example.Test2SpringBootExample21.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
}