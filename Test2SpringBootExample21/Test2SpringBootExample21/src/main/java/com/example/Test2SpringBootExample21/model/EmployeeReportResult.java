package com.example.Test2SpringBootExample21.model;

import jakarta.xml.bind.annotation.XmlRootElement;
import org.springframework.hateoas.RepresentationModel;

@XmlRootElement(name = "employee-report")
public class EmployeeReportResult extends RepresentationModel<EmployeeReportResult> {
}