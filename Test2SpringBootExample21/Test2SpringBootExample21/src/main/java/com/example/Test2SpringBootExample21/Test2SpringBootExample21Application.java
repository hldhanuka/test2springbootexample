package com.example.Test2SpringBootExample21;

import com.example.Test2SpringBootExample21.repository.EmployeeRepository;
import com.example.Test2SpringBootExample21.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.List;

@SpringBootApplication
//@ComponentScan({"com.example.Test2SpringBootExample21"})
//@EntityScan("com.example.Test2SpringBootExample21")
//@EnableJpaRepositories("com.example.Test2SpringBootExample21.repository")
public class Test2SpringBootExample21Application implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample21Application.class, args);
	}

	@Autowired
	EmployeeRepository repository;

	@Override
	public void run(String... args) throws Exception {
		repository.saveAll(List.of(
				new Employee(null, "Alex", "Dave", "alex@gmail.com"),
				new Employee(null, "Brian", "Dave", "brian@gmail.com"),
				new Employee(null, "Charles", "Dave", "charles@gmail.com")
			)
		);
	}

}
