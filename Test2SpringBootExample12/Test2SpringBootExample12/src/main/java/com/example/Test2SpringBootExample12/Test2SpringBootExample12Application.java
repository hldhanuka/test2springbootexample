package com.example.Test2SpringBootExample12;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test2SpringBootExample12Application {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample12Application.class, args);
	}

}