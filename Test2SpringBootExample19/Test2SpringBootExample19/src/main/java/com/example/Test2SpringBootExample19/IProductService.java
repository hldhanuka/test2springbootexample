package com.example.Test2SpringBootExample19;

import com.example.Test2SpringBootExample19.model.Product;

import java.util.List;

public interface IProductService {
    List<Product> findAll();
}