package com.example.Test2SpringBootExample24;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonVersoning3Controller {
    //this method is for first version that returns the entire name
    @GetMapping(value = "/person/header", headers = "X-API-Version=1")
    public PersonV1 personV1() {
        return new PersonV1("Tom Cruise");
    }

    //this method is for second version that returns firstName and lastName separately
    @GetMapping(value = "/person/header", headers = "X-API-Version=2")
    public PersonV2 personV2() {
        return new PersonV2(new Name("Tom", "Cruise"));
    }
}
