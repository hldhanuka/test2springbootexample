package com.example.Test2SpringBootExample24;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonVersoning2Controller {
    //this method is for first version that returns the entire name
    @GetMapping(value = "/person/param", params = "version=1")
    public PersonV1 personV1() {
        return new PersonV1("Tom Cruise");
    }

    //this method is for second version that returns firstName and lastName separately
    @GetMapping(value = "/person/param", params = "version=2")
    public PersonV2 personV2() {
        return new PersonV2(new Name("Tom", "Cruise"));
    }
}