package com.example.Test2SpringBootExample24;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test2SpringBootExample24Application {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample24Application.class, args);
	}

}
