package com.example.Test2SpringBootExample10;

import com.example.Test2SpringBootExample10.model.Account;
import com.example.Test2SpringBootExample10.service.impl.AccountService;
import com.example.Test2SpringBootExample10.service.impl.AccountServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
//@EnableAspectJAutoProxy annotation enables support for handling the components marked with @Aspect annotation. It is similar to tag in the xml configuration.  
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class Test2SpringBootExample10 {
    public static void main(String[] args) {
        ConfigurableApplicationContext ac = SpringApplication.run(Test2SpringBootExample10.class, args);
        //Fetching the account object from the application context
        AccountService accountService = ac.getBean("accountServiceImpl", AccountServiceImpl.class);
        Account account;
        try {
            //generating exception
            account = accountService.getAccountByCustomerId(null);
            if (account != null)
                System.out.println(account.getAccountNumber() + "\t" + account.getAccountType());
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}