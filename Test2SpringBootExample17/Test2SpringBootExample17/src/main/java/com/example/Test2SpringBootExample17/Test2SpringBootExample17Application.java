package com.example.Test2SpringBootExample17;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

//enables the cache management capability
@EnableCaching
@SpringBootApplication
public class Test2SpringBootExample17Application {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample17Application.class, args);
	}

}
