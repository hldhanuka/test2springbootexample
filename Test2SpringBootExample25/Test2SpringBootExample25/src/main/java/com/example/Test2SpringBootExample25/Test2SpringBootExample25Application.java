package com.example.Test2SpringBootExample25;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test2SpringBootExample25Application {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample25Application.class, args);
	}

}
