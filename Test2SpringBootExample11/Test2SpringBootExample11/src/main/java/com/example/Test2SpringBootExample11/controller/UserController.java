package com.example.Test2SpringBootExample11.controller;

import com.example.Test2SpringBootExample11.model.UserRecord;
import com.example.Test2SpringBootExample11.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("/")
    public List<UserRecord> getAllUser() {
        return userService.getAllUsers();
    }

    @RequestMapping(value = "/add-user", method = RequestMethod.POST, consumes = "application/json")
    public void addUser(@RequestBody UserRecord userRecord) {
        userService.addUser(userRecord);
    }
}