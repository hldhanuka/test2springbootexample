package com.example.Test2SpringBootExample11.repository;

import com.example.Test2SpringBootExample11.model.UserRecord;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserRecord, String> {
}