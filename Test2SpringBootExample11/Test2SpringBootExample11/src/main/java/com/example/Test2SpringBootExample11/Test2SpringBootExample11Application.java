package com.example.Test2SpringBootExample11;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test2SpringBootExample11Application {

	public static void main(String[] args) {
		SpringApplication.run(Test2SpringBootExample11Application.class, args);
	}

}
