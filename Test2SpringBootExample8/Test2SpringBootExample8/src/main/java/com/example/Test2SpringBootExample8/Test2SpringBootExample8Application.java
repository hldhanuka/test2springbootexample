package com.example.Test2SpringBootExample8;

import com.example.Test2SpringBootExample8.service.BankService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
//@EnableAspectJAutoProxy annotation enables support for handling the components marked with @Aspect annotation. It is similar to tag in the xml configuration.  
//@EnableAspectJAutoProxy
@EnableAspectJAutoProxy(proxyTargetClass=true)  
public class Test2SpringBootExample8Application {
    public static void main(String[] args) {
//        SpringApplication.run(Test2SpringBootExample8Application.class, args);
        
        ConfigurableApplicationContext context = SpringApplication.run(Test2SpringBootExample8Application.class, args);
        // Fetching the employee object from the application context.
        BankService bank = context.getBean(BankService.class);
        // Displaying balance in the account
        String accnumber = "12345";
        bank.displayBalance(accnumber);
        // Closing the context object
        context.close();
    }
}
